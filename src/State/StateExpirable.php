<?php

namespace Drupal\state_expirable\State;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\State\State;

/**
 * Class StateExpirable.
 *
 * Provides the state system using a key value store with expiration.
 *
 * Makes use of the KeyValueExpirable class to be able to cache State data
 * and set an expire time. When the expire time is reached the data in the
 * database is refreshed.
 */
class StateExpirable extends State implements StateExpirableInterface {

  /**
   * KeyValueExpirableFactoryInterface.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  private $keyValueExpirableFactory;

  /**
   * StateExpirable constructor.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   KeyValueFactoryInterface.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable_factory
   *   KeyValueExpirableFactoryInterface.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory, KeyValueExpirableFactoryInterface $key_value_expirable_factory) {
    $this->keyValueExpirableFactory = $key_value_expirable_factory->get('state');

    parent::__construct($key_value_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function setWithExpire($key, $value, $expire) {
    $this->cache[$key] = $value;
    $this->keyValueExpirableFactory->setWithExpire($key, $value, $expire);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultipleWithExpire(array $data, $expire) {
    foreach ($data as $key => $value) {
      $this->cache[$key] = $value;
    }
    $this->keyValueExpirableFactory->setMultipleWithExpire($data, $expire);
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(array $keys) {
    $values = [];
    $load = [];
    foreach ($keys as $key) {
      // Check if we have a value in the cache.
      if (isset($this->cache[$key])) {
        $values[$key] = $this->cache[$key];
      }
      // Load the value if we don't have an explicit NULL value.
      elseif (!array_key_exists($key, $this->cache)) {
        $load[] = $key;
      }
    }

    if ($load) {
      $loaded_values = $this->keyValueExpirableFactory->getMultiple($load);
      foreach ($load as $key) {
        // If we find a value, even one that is NULL, add it to the cache and
        // return it.
        if (isset($loaded_values[$key]) || array_key_exists($key, $loaded_values)) {
          $values[$key] = $loaded_values[$key];
          $this->cache[$key] = $loaded_values[$key];
        }
        else {
          $this->cache[$key] = NULL;
        }
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    foreach ($keys as $key) {
      unset($this->cache[$key]);
    }
    $this->keyValueExpirableFactory->deleteMultiple($keys);
  }

}
